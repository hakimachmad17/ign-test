<?php
//including the Function contain database connection file
include_once("classes/Function.php");

$function = new MyFunction();

$success  = false;

//retrieve data from api
$data = $function->CallOpenAPI('awmun');

//loop data for inserting data
foreach ($data as $key => $value) {
	$sql = 'INSERT INTO `user_data`(`nama`, `email`, `birth_date`, `country`, `phone`, `registration_date`, `need_pickup`, `motivation_letter`)'
		 .' VALUES ("'.$function->escape_string($value->nama).'","'.$value->email.'","'.$value->birth_date.'","'.$value->country.'","'.$value->phone.'","'.$value->registration_date.'",'.$value->need_pickup.',"'.$function->escape_string($value->motivation_letter).'")'
		 .' ON DUPLICATE KEY UPDATE `nama` = "'.$function->escape_string($value->nama).'", `birth_date` = "'.$value->birth_date.'", `country` = "'.$value->country.'", `phone` = "'.$value->phone.'", `registration_date` = "'.$value->registration_date.'", `need_pickup` = '.$value->need_pickup.', `motivation_letter` = "'.$function->escape_string($value->motivation_letter).'"';

	if ($function->execute($sql)) {
		$success = true;
	}
}

if ($success == true) {
	echo "Data retrieved successfully";
}