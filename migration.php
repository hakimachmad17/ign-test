<?php
$hostname = 'localhost';
$username = 'root';
$password = '';
$database = 'ign_test_hakim';

// Create connection
$conn = new mysqli($hostname, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = 'CREATE DATABASE IF NOT EXISTS '. $database;
if ($conn->query($sql) === TRUE) {
    echo "Database created successfully.";
} else {
    echo "Error creating database: " . $conn->error;
}
echo "\n";

$conn->close();

// Create connection
$conn = new mysqli($hostname, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = 'CREATE TABLE IF NOT EXISTS `user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `birth_date` date NOT NULL,
  `country` varchar(70) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `registration_date` date NOT NULL,
  `need_pickup` tinyint(1) NOT NULL,
  `motivation_letter` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';

if ($conn->query($sql) === TRUE) {
    echo "Table generated successfully.";
} else {
    echo "Error creating table: " . $conn->error;
}
echo "\n";

$conn->close();