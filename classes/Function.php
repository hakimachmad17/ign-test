<?php
include_once 'Config.php';

class MyFunction extends Config
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function getData($query)
	{		
		$result = $this->connection->query($query);
		
		if ($result == false) {
			return false;
		} 
		
		$rows = array();
		
		while ($row = $result->fetch_assoc()) {
			$rows[] = $row;
		}
		
		return $rows;
	}
		
	public function execute($query) 
	{
		$result = $this->connection->query($query);
		
		if ($result == false) {
			echo 'Error: cannot execute the command';
			return false;
		} else {
			return true;
		}		
	}
	
	public function delete($id, $table) 
	{ 
		$query = "DELETE FROM $table WHERE id = $id";
		
		$result = $this->connection->query($query);
	
		if ($result == false) {
			echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
			return false;
		} else {
			return true;
		}
	}
	
	public function escape_string($value)
	{
		return $this->connection->real_escape_string($value);
	}

	public function callAPI($method, $url, $data)
	{
	   $curl = curl_init();
	   $apiUrl = $this->apiUrl;

	   switch ($method){
	      case "POST":
	         curl_setopt($curl, CURLOPT_POST, 1);
	         if ($data)
	            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	         break;
	      case "PUT":
	         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
	         if ($data)
	            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
	         break;
	      default:
	         if ($data)
	            $url = sprintf("%s?%s", $url, http_build_query($data));
	   }

	   // OPTIONS:
	   curl_setopt($curl, CURLOPT_URL, $apiUrl.$url);
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	      'Content-Type: application/x-www-form-urlencoded',
	   ));
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	   // EXECUTE:
	   $result = curl_exec($curl);
	   if(!$result)
	   {
	   		die("Connection Failure");
	   }

	   curl_close($curl);
	   return $result;
	}

	public function CallOpenAPI($url, $option=NULL)
	{
		$apiUrl = $this->apiUrl;
		if ($option == NULL) {
			$opts = array(
			  'http'=>array(
			    'method'=>"GET",
			    'header'=>"Content-Type: application/x-www-form-urlencoded"
			  )
			);

			$context = stream_context_create($opts);
		}
		
		if (isset($option)) {
			$context = $option;
		}

		if (is_array($option)) {
			$context = stream_context_create($option);
		}

		$fp = fopen($apiUrl.$url, 'r', false, $context);

		$result = fgets($fp);
		$data = json_decode($result);

		fclose($fp);

		return $data;
	}
}
?>
