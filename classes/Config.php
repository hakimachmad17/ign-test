<?php  
class Config 
{	
	private $_hostname = 'localhost';
	private $_username = 'root';
	private $_password = '';
	private $_database = 'ign_test_hakim';
	
	protected $connection;

	public $apiUrl = 'https://api-test.internationalglobalnetwork.com/api/';
	
	public function __construct()
	{
		if (!isset($this->connection)) {
			
			$this->connection = new mysqli($this->_hostname, $this->_username, $this->_password, $this->_database);
			
			if (!$this->connection) {
				echo 'Cannot connect to database server';
				exit;
			}			
		}	
		
		return $this->connection;
	}
}
?>