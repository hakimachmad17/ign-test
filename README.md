# IGN Get Applicant Data Service

## Description
The service is built to process retrieving data event applicant through API. once service executed the retrieved data will be inserted into database, if data already exist it will be updated.

## Migrating data schema
**Important**, please execute `php migration.php` in command line for the first step to initialize database schema.

## Execute service
Please execute `php getApplicant.php` to start the service.

*Thanks for the opportunity*
**Achmad Hakim**
